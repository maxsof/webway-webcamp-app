document.querySelector(".js-subtask-open").addEventListener("click", (evt) => {
  evt.currentTarget.parentElement.parentElement.classList.add("subtask__add--open");
});

document.querySelector(".js-subtask-close").addEventListener("click", (evt) => {
  evt.currentTarget.parentElement.parentElement.classList.remove("subtask__add--open");
});
